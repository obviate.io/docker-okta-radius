# docker-okta-radius
Okta RADIUS server in a docker container

## Build Notes
* docker build -t okta-radius .
* Run:
  * touch config.properties
  * docker run --rm -it -v $PWD/config.properties:/opt/okta/ragent/user/config/radius/config.properties -e orgUrl=https://myorg.oktapreview.com -e sharedSecret=asdf1234 -e port=1812 -p 1812:1812/udp okta-radius
* docker run -it --rm 2stacks/radtest radtest first.last@myorg.com mypassword 10.99.99.5 0 asdf1234
