#!/bin/bash
if grep -q "ragent.version" /opt/okta/ragent/user/config/radius/config.properties; then
  # Found
  echo "config.properties already basic"
else
  echo "config.properties needs basic"
  cp /opt/okta/ragent/user/config/radius/blank-config.properties /opt/okta/ragent/user/config/radius/config.properties
fi

touch /home/OktaRadiusService/nohup.out
chown OktaRadiusService /home/OktaRadiusService/nohup.out
tail -f /home/OktaRadiusService/nohup.out &

if grep -q "ragent.okta.token" /opt/okta/ragent/user/config/radius/config.properties; then
  # Found
  echo "config.properties already setup - starting service"
  /etc/init.d/ragent start
else
  echo "config.properties needs setup"
  /opt/okta/ragent/scripts/configure.sh
fi

echo "## Done / Exiting ##"
#echo "### Start Log Tailing ###"
#tail -f /opt/okta/ragent/logs/okta_radius.log